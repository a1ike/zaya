$(document).ready(function () {

  $('.phone').inputmask('+7(999)999-99-99');

  $('.z-gallery__big').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: false,
    arrows: true,
    fade: true,
    asNavFor: '.z-gallery__small'
  });
  $('.z-gallery__small').slick({
    slidesToShow: 7,
    slidesToScroll: 1,
    asNavFor: '.z-gallery__big',
    dots: false,
    arrows: false,
    focusOnSelect: true,
    responsive: [{
      breakpoint: 1200,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: true,
        arrows: true,
        dots: false
      }
    }]
  });

  $('.z-order__card-minus').on('click', function (e) {
    var currentVal = $(this).next().val();
    if (currentVal > 1) {
      $(this).next().val(+currentVal - 1);
    }
  });

  $('.z-order__card-plus').on('click', function (e) {
    var currentVal = $(this).prev().val();
    $(this).prev().val(+currentVal + 1);
  });

  $('.z-item__minus').on('click', function (e) {
    var currentVal = $(this).next().val();
    if (currentVal > 1) {
      $(this).next().val(+currentVal - 1);
    }
  });

  $('.z-item__plus').on('click', function (e) {
    var currentVal = $(this).prev().val();
    $(this).prev().val(+currentVal + 1);
  });

  $('.z-item__collapse-header').on('click', function (e) {
    $(this).toggleClass('z-item__collapse-header_active');
    $(this).next().slideToggle('fast');
  });

  $('.open-popup').on('click', function (e) {
    e.preventDefault();
    $('.z-popup').slideToggle('fast');
  });

  $('.z-popup__close').on('click', function (e) {
    e.preventDefault();
    $('.z-popup').slideToggle('fast');
  });

});
